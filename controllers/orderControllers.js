const Order = require("../models/Order");


/*************************** Add a New Order *****************************/
module.exports.addOrder = (data) => {

  let newOrder = new Order (data.order);

  if(!data.isAdmin)
  return newOrder.save().then((result, error) => {
    console.log(result, error)
    if (error) {
      return false;
    }
    else {
      return { 
        data: newOrder,
        message: 'Order has been placed!' 
      };
    }
  });    
};


/**************************  Retrieving All Orders ***********************/
module.exports.getOrders = (data) => {

  if (data.isAdmin)
  return Order.find().then(
    (result, error) => {
      if (error) {
        return false;
      }
        return result;
      }
    ); 
};


/******************  Retrieving Authenticated User's Order ***************/
module.exports.getUserOrders = (data) => {

  if (!data.isAdmin)
  return Order.find({userId:userId}).then(
    (result, error) => {
      if (error) {
        return false;
      }
        return result;
      }
    ); 
};


/****************************  Cancel an Order *************************/
module.exports.cancelOrder = (data) => {

  return Order.findByIdAndUpdate(data.id, {orderStatus: 1}).then(
    (result, error) => {
      if (error) {
        return false;
      }
        console.log(result);
        return result;
      }
    ); 
};


